package com.lkakulia.lecture_12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val items = mutableListOf<ItemModel>()
    private lateinit var adapter: RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        addItems()
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items)
        recyclerView.adapter = adapter
    }

    private fun addItems() {
        items.add(ItemModel(R.drawable.one, "One", "This is a picture of number one"))
        items.add(ItemModel(R.drawable.two, "Two", "This is a picture of number two"))
        items.add(ItemModel(R.drawable.three, "Three", "This is a picture of number three"))
        items.add(ItemModel(R.drawable.four, "Four", "This is a picture of number four"))
        items.add(ItemModel(R.drawable.five, "Five", "This is a picture of number five"))
        items.add(ItemModel(R.drawable.six, "Six", "This is a picture of number six"))
        items.add(ItemModel(R.drawable.seven, "Seven", "This is a picture of number seven"))
        items.add(ItemModel(R.drawable.eight, "Eight", "This is a picture of number eight"))
        items.add(ItemModel(R.drawable.nine, "Nine", "This is a picture of number nine"))
        items.add(ItemModel(R.drawable.ten, "Ten", "This is a picture of number ten"))
    }
}
